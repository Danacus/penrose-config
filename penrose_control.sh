#!/bin/sh

case $@ in
    vol_up)       
        pactl set-sink-mute @DEFAULT_SINK@ false && pactl set-sink-volume @DEFAULT_SINK@ +5%
        ;;
    vol_down)
        pactl set-sink-mute @DEFAULT_SINK@ false && pactl set-sink-volume @DEFAULT_SINK@ -5%
        ;;            
    vol_mute)       
        pactl set-sink-mute @DEFAULT_SINK@ true
        ;;
    music_pause_toggle)
        mpc toggle
        ;;
    *)         
        pactl set-sink-mute @DEFAULT_SINK@ false && pactl set-sink-volume @DEFAULT_SINK@ -5%
        ;;            
esac
