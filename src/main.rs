#[macro_use]
extern crate penrose;

use penrose::{
    contrib::{extensions::Scratchpad, layouts::{paper, dwindle}},
    core::ring::Selector,
    core::xconnection::XConn,
    core::{
        hooks::Hooks,
        layout::{bottom_stack, monocle, side_stack, Layout, LayoutConf},
    },
    core::{
        helpers::{index_selectors, spawn, spawn_for_output},
        KeyEventHandler,
    },
    draw::*,
    draw::widget::{ActiveWindowName, CurrentLayout, RootWindowName, Workspaces},
    logging_error_handler, new_xcb_backed_window_manager,
    xcb::XcbDraw,
    Backward, Config, Forward, Less, More, Result, WindowManager,
};

use std::collections::HashMap;
use std::env;

use simplelog::{LevelFilter, SimpleLogger};

const HEIGHT: u32 = 16;

const FIRA: &'static str = "Hack Nerd Font";
const SERIF: &'static str = "Serif";

const BLACK: u32 = 0x282828ff;
const GREY: u32 = 0x6f838eff;
const WHITE: u32 = 0xffffffff;
const PURPLE: u32 = 0xb16286ff;
const BLUE: u32 = 0x458588ff;
const RED: u32 = 0xcc241dff;
const TRANSPARENT: u32 = 0x00000000;

const ICON_TERMINAL: &'static str = " ";
const ICON_BROWSER: &'static str = " ";
const ICON_CODE: &'static str = " ";
const ICON_GAME: &'static str = " ";

fn main() -> Result<()> {
    // penrose will log useful information about the current state of the WindowManager during
    // normal operation that can be used to drive scripts and related programs. Additional debug
    // output can be helpful if you are hitting issues.
    SimpleLogger::init(LevelFilter::Debug, simplelog::Config::default()).unwrap();

    // When specifying a layout, most of the time you will want LayoutConf::default() as shown
    // below, which will honour gap settings and will not be run on focus changes (only when
    // clients are added/removed). To customise when/how each layout is applied you can create a
    // LayoutConf instance with your desired properties enabled.
    let follow_focus_conf = LayoutConf {
        floating: false,
        gapless: true,
        follow_focus: true,
        allow_wrapping: false,
    };

    // Defauly number of clients in the main layout area
    let n_main = 1;

    // Default percentage of the screen to fill with the main area of the layout
    let ratio = 0.6;

    let config = Config::default()
        .builder()
        .workspaces([
            ICON_TERMINAL,
            ICON_BROWSER,
            ICON_CODE,
            "4",
            "5",
            "6",
            "7",
            "8",
            ICON_GAME,
        ])
        .floating_classes(["dmenu", "dunst", "polybar", "rofi"])
        .border_px(0)
        .gap_px(5)
        .bar_height(HEIGHT)
        .layouts(vec![
            Layout::new("[dwdl]", LayoutConf::default(), dwindle, n_main, ratio),
            Layout::new("[side]", LayoutConf::default(), side_stack, n_main, ratio),
            Layout::new("[botm]", LayoutConf::default(), bottom_stack, n_main, ratio),
            Layout::new("[papr]", follow_focus_conf, paper, n_main, ratio),
            Layout::new("[full]", LayoutConf::default(), monocle, n_main, ratio),
            Layout::floating("[----]"),
        ])
        .build()
        .expect("Invalid configuration");

    let my_program_launcher = "rofi -show drun";
    let my_file_manager = "dolphin";
    let my_terminal = "alacritty";

    let home = env::var("HOME").unwrap();
    let script = format!("{}/.config/penrose/exit.sh", home);
    let power_menu = Box::new(move |wm: &mut WindowManager<_>| {
        match spawn_for_output(&script) {
            Ok(o) => match o.as_str() {
                "logout\n" => wm.exit(),
                _ => Ok(()), // other options are handled by the script
            },
            Err(_) => Ok(()),
        }
    });

    /* hooks
     *
     * penrose provides several hook points where you can run your own code as part of
     * WindowManager methods. This allows you to trigger custom code without having to use a key
     * binding to do so. See the hooks module in the docs for details of what hooks are avaliable
     * and when/how they will be called. Note that each class of hook will be called in the order
     * that they are defined. Hooks may maintain their own internal state which they can use to
     * modify their behaviour if desired.
     */
    let mut hooks: Hooks<_> = Vec::new();

    hooks.push(Box::new(setup_bar(
        XcbDraw::new()?,
        HEIGHT as usize,
        &TextStyle {
            font: FIRA.to_string(),
            point_size: 9,
            fg: WHITE.into(),
            bg: None,
            padding: (2.0, 2.0),
        },
        GREY,  // highlight
        WHITE, // empty_ws
        config.workspaces().as_slice(),
    )?));

    // Scratchpad is an extension: it makes use of the same Hook points as the examples above but
    // additionally provides a 'toggle' method that can be bound to a key combination in order to
    // trigger the bound scratchpad client.
    //let sp = Scratchpad::new("alacritty", 0.8, 0.8, AnchoredPosition::default());
    let sp = Scratchpad::new("alacritty", 0.8, 0.8);
    hooks.push(sp.get_hook());

    //let mpc_rs = Scratchpad::new("alacritty -e mpc-rs", 0.35, 0.3, AnchoredPosition::new(Anchor::Center, Anchor::Bottom, 0, 0));
    let mpc_rs = Scratchpad::new("alacritty -e mpc-rs", 0.35, 0.3);
    hooks.push(mpc_rs.get_hook());

    //let htop = Scratchpad::new("alacritty -e htop", 0.25, 0.22, AnchoredPosition::new(Anchor::Left, Anchor::Top, 100, 50));
    let htop = Scratchpad::new("alacritty -e htop", 0.25, 0.22);
    hooks.push(htop.get_hook());

    /* The gen_keybindings macro parses user friendly key binding definitions into X keycodes and
     * modifier masks. It uses the 'xmodmap' program to determine your current keymap and create
     * the bindings dynamically on startup. If this feels a little too magical then you can
     * alternatively construct a  HashMap<KeyCode, FireAndForget> manually with your chosen
     * keybindings (see helpers.rs and data_types.rs for details).
     * FireAndForget functions do not need to make use of the mutable WindowManager reference they
     * are passed if it is not required: the run_external macro ignores the WindowManager itself
     * and instead spawns a new child process.
     */
    let key_bindings = gen_keybindings! {
        // Program launch
        "M-d" => run_external!(my_program_launcher);
        "M-Return" => run_external!(my_terminal);

        // client management
        "M-j" => run_internal!(cycle_client, Forward);
        "M-k" => run_internal!(cycle_client, Backward);
        "M-S-j" => run_internal!(drag_client, Forward);
        "M-S-k" => run_internal!(drag_client, Backward);
        "M-S-q" => run_internal!(kill_client);
        "M-f" => run_internal!(toggle_client_fullscreen, &Selector::Focused);

        "M-w" => sp.toggle();
        "M-S-m" => mpc_rs.toggle();
        "M-F2" => htop.toggle();

        // workspace management
        "M-Tab" => run_internal!(toggle_workspace);
        "M-bracketright" => run_internal!(cycle_screen, Forward);
        "M-bracketleft" => run_internal!(cycle_screen, Backward);
        "M-S-bracketright" => run_internal!(drag_workspace, Forward);
        "M-S-bracketleft" => run_internal!(drag_workspace, Backward);

        // Layout management
        "M-grave" => run_internal!(cycle_layout, Forward);
        "M-S-grave" => run_internal!(cycle_layout, Backward);
        "M-A-Up" => run_internal!(update_max_main, More);
        "M-A-Down" => run_internal!(update_max_main, Less);
        "M-A-Right" => run_internal!(update_main_ratio, More);
        "M-A-Left" => run_internal!(update_main_ratio, Less);

        "M-A-s" => run_internal!(detect_screens);

        // General purpose commands
        "XF86AudioRaiseVolume" => run_control_command("vol_up");
        "XF86AudioLowerVolume" => run_control_command("vol_down");
        "XF86AudioMute" => run_control_command("vol_mute");
        "M-m" => run_control_command("music_pause_toggle");

        "M-S-e" => power_menu;
        "M-A-Escape" => run_internal!(exit);

        // Each keybinding here will be templated in with the workspace index of each workspace,
        // allowing for common workspace actions to be bound at once.
        map: { "1", "2", "3", "4", "5", "6", "7", "8", "9" } to index_selectors(9) => {
            "M-{}" => focus_workspace(REF);
            "M-S-{}" => client_to_workspace(REF);
        };
    };

    // Create the WindowManager instance with the config we have built and a connection to the X
    // server. Before calling grab_keys_and_run, it is possible to run additional start-up actions
    // such as configuring initial WindowManager state, running custom code / hooks or spawning
    // external processes such as a start-up script.
    let mut wm = new_xcb_backed_window_manager(config, hooks, logging_error_handler())?;

    spawn(format!("{}/.config/penrose/autorun.sh", home))?;

    // grab_keys_and_run will start listening to events from the X server and drop into the main
    // event loop. From this point on, program control passes to the WindowManager so make sure
    // that any logic you wish to run is done before here!
    wm.grab_keys_and_run(key_bindings, HashMap::new())?;

    Ok(())
}

const MAX_ACTIVE_WINDOW_CHARS: usize = 80;

fn run_control_command<X: XConn>(cmd: &'static str) -> KeyEventHandler<X> {
    let home = env::var("HOME").unwrap();

    Box::new(move |_: &mut WindowManager<X>| {
        spawn(format!(
            "{}/.config/penrose/penrose_control.sh {}",
            home, cmd
        ))
    }) as KeyEventHandler<X>
}

fn setup_bar<C, D, X>(
    drw: D,
    height: usize,
    style: &TextStyle,
    highlight: impl Into<Color>,
    empty_ws: impl Into<Color>,
    workspaces: &[String],
) -> Result<StatusBar<C, D, X>>
where
    C: DrawContext + 'static,
    D: Draw<Ctx = C>,
    X: XConn,
{
    let highlight = highlight.into();

    Ok(StatusBar::try_new(
        drw,
        Position::Top,
        height,
        style.bg.unwrap_or(TRANSPARENT.into()),
        &[&style.font],
        vec![
            Box::new(Workspaces::new(workspaces, style, highlight, empty_ws)),
            Box::new(CurrentLayout::new(style)),
            Box::new(ActiveWindowName::new(
                &TextStyle {
                    padding: (6.0, 4.0),
                    ..style.clone()
                },
                MAX_ACTIVE_WINDOW_CHARS,
                true,
                false,
            )),
            Box::new(RootWindowName::new(style, false, true)),
        ],
    )?)
}
