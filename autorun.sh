#!/usr/bin/env bash

function run {
  if ! pgrep -f $1 ;
  then
    $@&
  fi
}

run xsetroot -cursor_name left_ptr
run xset dpms 0 0 0 && xset s noblank && xset s off
run ~/.config/picom/launch.sh
#run ~/.config/awesome/autohidewibox.py ~/.config/awesome/autohidewibox.conf
run ~/setbg.sh 
run dunst
run pulseeffects --gapplication-service
xsetwacom set "Wacom HID 50DB Finger touch" Gesture off
run touchegg
run nm-applet
run kdeconnect-indicator
#run music_wake.sh

run ~/.config/penrose/penrose_stat.zsh
