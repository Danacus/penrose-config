#!/bin/sh

ANSWER=$(echo -e "suspend\nlock\nlogout\nshutdown\nreboot" | rofi -dmenu)

case $ANSWER in
	"suspend") systemctl suspend;;
	"lock") xset s activate;;
    "logout") echo 'logout';;
	"shutdown") shutdown now;;
	"reboot") reboot;;
esac
